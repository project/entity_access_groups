INTRODUCTION
------------

The Entity Access Groups module allows site-builders to restrict entity access operations (view, create, update, delete) based on an entity relationship where the same entity bundles are referenced on both the content and user entity.

The module was built so the content entity doesn't require any field reconstruction, so the module works with existing entity references that have already been defined. You'll need to create an Entity Access Group and select the references to use for both the content and user entities.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * First, you'll need to attach an entity reference field to  the content entity that needs to have restricted access.

    * The following field widgets are supported:
        - options_select
        - options_buttons
        - multiple_options_select

        The field widget restriction is only if you choose to show relevant user access groups they've been assigned.

 * Next, you'll need to attach an entity reference field to the user entity, which references the same bundles as the entity reference that was attached on the content entity.

 * Navigate to `/admin/config/content/entity-access-groups` where you'll be showing a listing of all entity access groups, but nothing should be there initially.

 * Click on `Add Access Group` to create a new access group configuration.

 * Input the required field values based on the entity access requirements for the entity type / bundles, and click `Save`.

 * Update the permissions (`/admin/people/permissions`) and assign any roles with the ability to manage the entity access group which you've created.

 * Any users with those roles will be able to add/remove users that are associated to the access group, if you navigate to the referenced content you should see the `Access Group` tab within the local task menu.

* Based on the entity access operation you should now be able to verify that when a user is not apart of an access group the content is restricted.

MAINTAINERS
-----------

Current maintainers:
 * Travis Tomka (droath) - https://www.drupal.org/u/droath
