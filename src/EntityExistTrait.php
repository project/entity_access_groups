<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Define the entity exist trait.
 */
trait EntityExistTrait {

  /**
   * Determine if entity exists based on provided identifier.
   *
   * @param string $identifier
   *   The entity identifier to check.
   *
   * @return bool
   *   Return TRUE if entity exist for that identifier; otherwise FALSE.
   */
  public function exists(string $identifier): bool {
    $query = $this->entityStorage()->getQuery();
    $query->condition($this->getEntityType()->getKey('id'), $identifier);
    return (bool) $query->accessCheck()->execute();
  }

  /**
   * Get the entity storage instance.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   */
  public function entityStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage($this->getEntityTypeId());
  }
}
