<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Routing;

use Symfony\Component\Routing\Route;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_access_groups\Form\EntityAccessGroupUserAddForm;
use Drupal\entity_access_groups\Form\EntityAccessGroupUserRemoveForm;

/**
 * Define the entity access groups routing.
 */
class EntityAccessGroupsRouting {

  use StringTranslationTrait;

  /**
   * The entity access groups routes.
   *
   * @return array
   *   An array of the entity access groups routes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function routes(): array {
    $routes = [];

    /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $access_group */
    foreach ($this->loadAllEntityAccessGroups() as $name => $access_group) {
      $route = (new Route('/admin/entity-access-group/{entity_access_group}/node/{node}/user/add'))
        ->addDefaults([
          '_title' => 'Add User',
          '_form' => EntityAccessGroupUserAddForm::class
        ])
        ->addOptions([
          'parameters' => [
            'node' => [
              'type' => 'entity:node'
            ],
            'entity_access_group' => [
              'type' => 'entity:entity_access_group'
            ]
          ]
        ])
      ->addRequirements([
        'node' => '\d+',
        '_permission' => $access_group->getPermissionName()
      ]);

      $routes["entity_access_group.manage.{$name}.add_user"] = $route;

      $route = (new Route('/admin/entity-access-group/{entity_access_group}/node/{node}/user/{user}/remove'))
        ->addDefaults([
          '_title' => 'Remove User',
          '_form' => EntityAccessGroupUserRemoveForm::class
        ])
        ->addOptions([
          'parameters' => [
            'node' => [
              'type' => 'entity:node'
            ],
            'user' => [
              'type' => 'entity:user'
            ],
            'entity_access_group' => [
              'type' => 'entity:entity_access_group'
            ]
          ]
        ])
        ->addRequirements([
          'node' => '\d+',
          '_permission' => $access_group->getPermissionName()
        ]);

      $routes["entity_access_group.manage.{$name}.remove_user"] = $route;
    }

    return $routes;
  }

  /**
   * Get user account instance.
   *
   * @return \Drupal\Core\Session\AccountProxyInterface
   */
  protected function getAccount(): AccountProxyInterface {
    return \Drupal::currentUser();
  }

  /**
   * Load all entity access groups.
   *
   * @return array
   *   An array of all entity access groups.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadAllEntityAccessGroups(): array {
    return $this->getEntityAccessGroupStorage()->loadMultiple();
  }

  /**
   * Get entity access group storage instance.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityAccessGroupStorage(): EntityStorageInterface {
    return \Drupal::entityTypeManager()->getStorage('entity_access_group');
  }
}
