<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Contract;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Define the entity access group interface.
 */
interface EntityAccessGroupInterface extends ConfigEntityInterface {

  /**
   * Get entity access group operations.
   *
   * @return array
   *   An array of access operations.
   */
  public function operations(): array;

  /**
   * Get entity access group entity type.
   *
   * @return string
   *   The entity access entity type.
   */
  public function entityType(): string;

  /**
   * Get entity access group bundles.
   *
   * @return array
   *   An array of the entity bundles.
   */
  public function entityBundles(): array;

  /**
   * Determine if access group has bundles.
   *
   * @return bool
   *   Return TRUE entity has bundles; otherwise FALSE.
   */
  public function hasEntityBundles(): bool;

  /**
   * Get entity access reference field.
   *
   * @return string
   *   Return entity access reference field name.
   */
  public function entityReferenceField(): string;

  /**
   * Get entity access reference field options.
   *
   * @return string
   *   Get the user entity reference field option.
   */
  public function entityReferenceFieldOption(): string;

  /**
   * Get entity access reference field required.
   *
   * @return bool
   *   Get the entity reference field required.
   */
  public function entityReferenceFieldRequired(): bool;

  /**
   * Get user access reference field.
   *
   * @return string
   *   Get the user access reference field name.
   */
  public function userReferenceField(): string;

  /**
   * Get user access reference field access.
   *
   * @return string
   *   Get user reference field access.
   */
  public function userReferenceFieldAccess(): string;

  /**
   * Get user access referenced content access restrict.
   *
   * @return bool
   *   Determine if referenced content should be restricted; otherwise FALSE.
   */
  public function userReferencedContentAccessRestrict(): bool;

  /**
   * Get user access referenced content access operations.
   *
   * @return array
   *   An array of operations to restrict access on for the referenced content.
   */
  public function userReferencedContentAccessOperations(): array;

  /**
   * Get the entity access group permission name.
   *
   * @return string
   */
  public function getPermissionName(): string;
}
