<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Contract;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Define the entity access groups handler interface.
 */
interface EntityAccessGroupsHandlerInterface {

  /**
   * Check entity based on access group definitions.
   *
   * @param string $operation
   *   The access check operation.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity instance.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account instance.
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function checkAccess(
    string $operation,
    EntityInterface $entity,
    AccountInterface $account
  ): AccessResult;

  /**
   * Alter the entity edit form based on the entity access groups.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   */
  public function alterEntityEditForm(
    array &$form,
    FormStateInterface $form_state
  ): void;

  /**
   * Alter the user edit form based on the entity access groups.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   */
  public function alterUserEditForm(
    array &$form,
    FormStateInterface $form_state
  ): void;
}
