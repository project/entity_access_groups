<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups;

use Drupal\user\Entity\User;
use Drupal\Core\Render\Element;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResultNeutral;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\entity_access_groups\Form\EntityAccessGroupDefaultForm;
use Drupal\entity_access_groups\Contract\EntityAccessGroupInterface;
use Drupal\entity_access_groups\Contract\EntityAccessGroupsHandlerInterface;

/**
 * Define the entity access groups handler.
 */
class EntityAccessGroupsHandler implements EntityAccessGroupsHandlerInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Entity access groups handler constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   */
  public function __construct(
    AccountProxyInterface $account,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritDoc}
   */
  public function checkAccess(
    string $operation,
    EntityInterface $entity,
    AccountInterface $account
  ): AccessResult {
    try {
      /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $access_group */
      foreach ($this->loadAllEntityAccessGroups() as $access_group) {
        $access_result = $this->doFullEntityAccessGroupCheck(
          $operation,
          $entity,
          $account,
          $access_group
        );

        if (!$access_result instanceof AccessResultNeutral) {
          return $access_result;
        }
      }
    } catch (\Exception $exception) {
      watchdog_exception('entity_access_groups', $exception);
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritDoc}
   */
  public function alterEntityEditForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $form_object = $form_state->getFormObject();

    if (!$form_object instanceof ContentEntityFormInterface) {
      return;
    }
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_object->getEntity();

    try {
      $access_groups = $this->loadEntityAccessGroups($entity->getEntityTypeId());

      /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $access_group */
      foreach ($access_groups as $access_group) {
        if ($access_group->hasEntityBundles()
          && !in_array($entity->bundle(), $access_group->entityBundles())) {
          continue;
        }
        $reference_field = $access_group->entityReferenceField();

        if (!$entity->hasField($reference_field)) {
          continue;
        }
        $reference_widget = &$form[$reference_field]['widget'];
        $reference_widget['#required'] = $access_group->entityReferenceFieldRequired();

        switch ($access_group->entityReferenceFieldOption()) {
          case EntityAccessGroupDefaultForm::ENTITY_REFERENCE_OPTION_RELEVANT:
            if (!$this->accountIsAdmin() && !$this->accountIsManager($access_group)) {
              $component = $this->getEntityFormDisplay($entity)
                ->getComponent($reference_field);

              $this->alterWidgetRelevantOptions(
                $reference_widget,
                $component,
                $access_group
              );
            }
            break;
        }
      }
    } catch (\Exception $exception) {
      watchdog_exception('entity_access_groups', $exception);
    }
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   */
  protected function getEntityFormDisplay(
    ContentEntityInterface $entity
  ): EntityFormDisplayInterface {
    return $this->entityDisplayRepository
      ->getFormDisplay($entity->getEntityTypeId(), $entity->bundle());
  }

  /**
   * {@inheritDoc}
   */
  public function alterUserEditForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $form_object = $form_state->getFormObject();

    if (!$form_object instanceof ContentEntityFormInterface) {
      return;
    }
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_object->getEntity();

    try {
      /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $access_group */
      foreach ($this->loadAllEntityAccessGroups() as $access_group) {
        $reference_field = $access_group->userReferenceField();

        if (!$entity->hasField($reference_field)) {
          continue;
        }
        $reference_widget = &$form[$reference_field]['widget'];
        $reference_access = $access_group->userReferenceFieldAccess();

        switch ($reference_access) {
          case EntityAccessGroupDefaultForm::USER_REFERENCE_ACCESS_ONLY_ADMIN:
            if (!$this->accountIsAdmin() && !$this->accountIsManager($access_group)) {
              $reference_widget['#disabled'] = TRUE;
            }
            break;
        }
      }
    } catch (\Exception $exception) {
      watchdog_exception('entity_access_groups', $exception);
    }
  }

  /**
   * Entity access group widget validate callback.
   *
   * @param array $widget
   *   An array of the field widget.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   */
  public function entityAccessGroupWidgetValidate(
    array $widget,
    FormStateInterface $form_state
  ): void {
    $hidden_values = $widget['#entity_access_groups']['hidden_values'] ?? [];

    if (empty($hidden_values) || !is_array($widget['#value'])) {
      return;
    }
    $form_state->setValueForElement(
      $widget,
      array_merge($widget['#value'], $hidden_values)
    );
  }

  /**
   * Alter field widget relevant options.
   *
   * @param array $widget
   *   An array of the field widget.
   * @param array $component
   *   An array of the field form display component.
   * @param \Drupal\entity_access_groups\Contract\EntityAccessGroupInterface $access_group
   *   The access group instance.
   */
  protected function alterWidgetRelevantOptions(
    array &$widget,
    array $component,
    EntityAccessGroupInterface $access_group
  ): void {
    $deltas = Element::children($widget);

    $user_reference_values = $this->getUserReferenceValues($access_group);

    if (count($deltas) > 0) {
      foreach ($deltas as $delta) {
        if (!is_integer($delta)) {
          continue;
        }
        $item = &$widget[$delta];

        switch ($component['type']) {
          case 'multiple_options_select':
            $item_default_value = $item['target_id']['#default_value'];

            if (isset($item_default_value)
              && is_numeric($item_default_value)
              && !in_array($item_default_value, $user_reference_values)) {
              $item['#disabled'] = TRUE;
            }
            else {
              $item['target_id']['#options'] = [
                  '_none' => $this->t('- Select -')
                ] + $this->filterOptionsByUserReferenceValues(
                  $item['target_id']['#options'],
                  $access_group
                );
            }
            break;
        }
      }
    }
    else {
      $widget_default_values = $widget['#default_value'] ?? [];

      switch ($component['type']) {
        case 'options_select':
        case 'options_buttons':
          if (isset($widget['#options'])) {
            $widget['#options'] = $this->filterOptionsByUserReferenceValues(
              $widget['#options'],
              $access_group
            );
          }
          break;
      }
      $widget['#entity_access_groups']['hidden_values'] = array_diff(
        $widget_default_values, $user_reference_values
      );
      $widget['#element_validate'] = [
        [$this, 'entityAccessGroupWidgetValidate']
      ];
    }
  }

  /**
   * Conduct the full entity access group check.
   *
   * @param string $operation
   *   The entity operation.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity instance.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account instance.
   * @param \Drupal\entity_access_groups\Contract\EntityAccessGroupInterface $access_group
   *   The entity access group instance.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function doFullEntityAccessGroupCheck(
    string $operation,
    EntityInterface $entity,
    AccountInterface $account,
    EntityAccessGroupInterface $access_group
  ): AccessResultInterface {
    $user_reference_access = $this->checkUserReferencedContentAccess(
      $operation,
      $entity,
      $account,
      $access_group
    );

    if (!$user_reference_access instanceof AccessResultNeutral) {
      return $user_reference_access;
    }

    $content_reference_access = $this->checkEntityReferencedContentAccess(
      $operation,
      $entity,
      $account,
      $access_group
    );

    if (!$content_reference_access instanceof AccessResultNeutral) {
      return $content_reference_access;
    }

    return AccessResult::neutral();
  }

  /**
   * Check user referenced content access.
   *
   * @param string $operation
   *   The entity operation.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity instance.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account instance.
   * @param \Drupal\entity_access_groups\Contract\EntityAccessGroupInterface $access_group
   *   The entity access group instance.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function checkUserReferencedContentAccess(
    string $operation,
    EntityInterface $entity,
    AccountInterface $account,
    EntityAccessGroupInterface $access_group
  ): AccessResultInterface {
    if ($access_group->userReferencedContentAccessRestrict()
      && in_array($operation, $access_group->userReferencedContentAccessOperations())) {
      $user = User::load($account->id());
      $user_reference_field = $access_group->userReferenceField();

      if ($user->hasField($user_reference_field)) {
        /** @var \Drupal\Core\Field\FieldItemListInterface $user_reference_item */
        $user_reference_item = $user->get($user_reference_field);
        $user_reference_type = $user_reference_item->getSetting('target_type');
        $user_reference_bundles = $user_reference_item->getSetting('handler_settings')['target_bundles'] ?? [];

        if ($user_reference_type == $entity->getEntityTypeId()
          && in_array($entity->bundle(), $user_reference_bundles)) {

          $user_reference_values = $this->getFieldItemMainPropertyValues(
            $user_reference_item
          );

          if ($this->accountIsAdmin()
            || $this->accountIsManager($access_group)
            || in_array($entity->id(), $user_reference_values)) {
            return AccessResult::allowed();
          }

          return AccessResult::forbidden();
        }
      }
    }

    return AccessResult::neutral();
  }

  /**
   * Check entity referenced content access.
   *
   * @param string $operation
   *   The entity operation.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity instance.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account instance.
   * @param \Drupal\entity_access_groups\Contract\EntityAccessGroupInterface $access_group
   *   The entity access group instance.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result instance.
   */
  protected function checkEntityReferencedContentAccess(
    string $operation,
    EntityInterface $entity,
    AccountInterface $account,
    EntityAccessGroupInterface $access_group
  ): AccessResultInterface {
    if (in_array($operation, $access_group->operations())
      && $access_group->entityType() === $entity->getEntityTypeId()
      && in_array($entity->bundle(), $access_group->entityBundles())) {

      if ($account->hasPermission('bypass entity access group restrictions')
        || $this->accountIsManager($access_group)) {
        return AccessResult::allowed();
      }
      /** @var \Drupal\user\Entity\User $user */
      $user = User::load($account->id());
      $content_field = $entity->get($access_group->entityReferenceField());

      if (!$content_field->isEmpty()) {
        $user_field = $user->get($access_group->userReferenceField());

        return $this->hasFieldPropertyValue($user_field, $content_field)
          ? AccessResult::allowed()
          : AccessResult::forbidden();
      }
    }

    return AccessResult::neutral();
  }

  /**
   * Filter options by user reference values.
   *
   * @param array $options
   *   An array of options to filter out user reference values.
   * @param \Drupal\entity_access_groups\Contract\EntityAccessGroupInterface $access_group
   *   The entity access group instance.
   *
   * @return array
   *   An array of options based on the access group user reference values.
   */
  protected function filterOptionsByUserReferenceValues(
    array $options,
    EntityAccessGroupInterface $access_group
  ): array {
    return array_intersect_key(
      $options,
      array_flip($this->getUserReferenceValues($access_group))
    );
  }

  /**
   * Determine if current account is admin.
   *
   * @return bool
   *   Return TRUE if the account is an admin; otherwise FALSE.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function accountIsAdmin(): bool {
    if ($this->account->id() == 1) {
      return TRUE;
    }

    return in_array($this->findAdminRoleName(), $this->account->getRoles());
  }

  /**
   * Determine if current account is a manager.
   *
   * @param \Drupal\entity_access_groups\Contract\EntityAccessGroupInterface $access_group
   *   The entity access group instance.
   *
   * @return bool
   *   Return TRUE if the account is an manager; otherwise FALSE.
   */
  protected function accountIsManager(
    EntityAccessGroupInterface $access_group
  ): bool {
    if ($this->account->hasPermission('manage all entity access groups')) {
      return TRUE;
    }

    if ($this->account->hasPermission($access_group->getPermissionName())) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Find the role that is used as the administrator role.
   *
   * @return string
   *   The administrator role.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function findAdminRoleName(): string {
    $admin_roles = array_keys($this->entityTypeManager
      ->getStorage('user_role')
      ->loadByProperties(['is_admin' => TRUE]));

    return reset($admin_roles);
  }

  /**
   * Get the access group user reference values.
   *
   * @param \Drupal\entity_access_groups\Contract\EntityAccessGroupInterface $access_group
   *   The entity access group instance.
   *
   * @return array
   *   An array of current user references values.
   */
  protected function getUserReferenceValues(
    EntityAccessGroupInterface $access_group
  ): array {
    $user = User::load($this->account->id());
    $field_name = $access_group->userReferenceField();

    if (!$user->hasField($field_name)) {
      return [];
    };

    /** @var \Drupal\Core\Field\FieldItemList $field_item */
    $field_item = $user->get($field_name);

    return $this->getFieldItemMainPropertyValues($field_item);
  }

  /**
   * Get field item main property values.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field_item
   *   The field item instance.
   *
   * @return array
   *   An array of field item main property values.
   */
  protected function getFieldItemMainPropertyValues(
    FieldItemListInterface $field_item
  ): array {
    $values = [];
    $main_property = $field_item->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getMainPropertyName();

    foreach ($field_item->getValue() as $value) {
      if (!isset($value[$main_property])) {
        continue;
      }
      $values[] = $value[$main_property];
    }

    return $values;
  }

  /**
   * Has field property value in the haystack.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $needle
   * @param \Drupal\Core\Field\FieldItemListInterface $haystack
   *
   * @return bool
   *   If needle field value is found in the field haystack then return true,
   *   otherwise false.
   */
  protected function hasFieldPropertyValue(
    FieldItemListInterface $needle,
    FieldItemListInterface $haystack
  ): bool {
    $needle_definition = $needle->getFieldDefinition();
    $haystack_definition = $haystack->getFieldDefinition();

    if ($needle_definition->getType() === $haystack_definition->getType()) {
      $needle_name = $needle_definition
        ->getFieldStorageDefinition()
        ->getMainPropertyName();

      $haystack_name = $haystack_definition
        ->getFieldStorageDefinition()
        ->getMainPropertyName();

      foreach ($needle->getValue() as $needle_value) {
        foreach ($haystack->getValue() as $haystack_value) {
          if ($needle_value[$needle_name] === $haystack_value[$haystack_name]) {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * Load entity access groups by entity type.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of the entity access groups.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadEntityAccessGroups(string $entity_type_id): array {
    return $this->entityAccessGroupStorage()->loadByProperties([
      'entity_type' => $entity_type_id,
    ]);
  }

  /**
   * Load all entity access groups.
   *
   * @return array
   *   An array of all entity access groups.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadAllEntityAccessGroups(): array {
    return $this->entityAccessGroupStorage()->loadMultiple();
  }

  /**
   * Get entity access group storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function entityAccessGroupStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('entity_access_group');
  }
}
