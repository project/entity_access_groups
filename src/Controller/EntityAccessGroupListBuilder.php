<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;

/**
 * Define entity access group list builder.
 */
class EntityAccessGroupListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function buildHeader(): array {
    return [
      'label' => $this->t('Label')
    ] + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity): array {
    return [
      'label' => $entity->label()
    ] + parent::buildRow($entity);
  }
}
