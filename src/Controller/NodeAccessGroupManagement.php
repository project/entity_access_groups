<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Controller;

use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\node\NodeInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Define the node access group management controller.
 */
class NodeAccessGroupManagement extends ControllerBase {

  /**
   * Define the local task group management.
   *
   * @param \Drupal\node\NodeInterface|null $node
   *
   * @return array
   *   An array of the node access group management.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function content(NodeInterface $node = NULL): array {
    $build = [];

    $account = $this->currentUser();
    $user = User::load($account->id());

    /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $access_group */
    foreach ($this->getNodeAccessGroups() as $name => $access_group) {
      if (!$account->hasPermission($access_group->getPermissionName())) {
        continue;
      }
      $reference_field = $access_group->userReferenceField();

      if (!$user->hasField($reference_field)) {
        continue;
      }
      $reference_item = $user->get($reference_field);

      if ($reference_item->getSetting('target_type') !== $node->getEntityTypeId()) {
        continue;
      }
      $reference_bundles = $reference_item
        ->getSetting('handler_settings')['target_bundles'] ?? [];

      if (in_array($node->bundle(), $reference_bundles)) {
        $user_storage = $this->entityTypeManager()->getStorage('user');

        $user_ids = $user_storage->getQuery()
          ->accessCheck()
          ->condition("{$reference_field}.target_id", $node->id())
          ->condition('status', NodeInterface::PUBLISHED)
          ->execute();

        $build[$name] = [
          '#type' => 'details',
          '#title' => $this->t(
            '@label Access Group', ['@label' => $access_group->label()]
          ),
          '#open' => TRUE,
        ];
        $build[$name]['users'] = [
          '#type' => 'table',
          '#header' => [
            $this->t('Username'),
            $this->t('Access Operations'),
            $this->t('Manage Operations')
          ],
          '#empty' => $this->t(
            'There are no users associated with the entity access group.'
          )
        ];

        /** @var \Drupal\user\Entity\User $user */
        foreach ($user_storage->loadMultiple($user_ids) as $id => $user) {
          $build[$name]['users'][$id]['username']['#plain_text'] = $user->getAccountName();
          $build[$name]['users'][$id]['access_operations']['#plain_text'] = implode(', ', $access_group->operations());
          $build[$name]['users'][$id]['manage_operations']['remove_user'] = [
            '#type' => 'link',
            '#url' => Url::fromRoute("entity_access_group.manage.{$name}.remove_user", [
              'user' => $id,
              'node' => $node->id(),
              'entity_access_group' => $name,
            ]),
            '#title' => $this->t('Remove'),
            '#attributes' => [
              'class' => ['use-ajax'],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => '{ "width": 800 }',
            ],
          ];
        }

        $build[$name]['add_user'] = [
          '#type' => 'link',
          '#url' => Url::fromRoute("entity_access_group.manage.{$name}.add_user", [
            'node' => $node->id(),
            'entity_access_group' => $name
          ]),
          '#title' => $this->t('Add User'),
          '#attributes' => [
            'class' => ['use-ajax'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => '{ "width": 800 }'
          ]
        ];
      }
      else {
        $build['#markup'] = $this->t(
          'There are no entity access group references for this content type.'
        );
      }
    }

    if (count(Element::children($build)) > 0) {
      $build['#attached']['library'][] = 'core/jquery.form';
      $build['#attached']['library'][] = 'core/drupal.dialog.ajax';
    }

    return $build;
  }

  /**
   * Check access for the node access group management content.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResult
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(AccountInterface $account): AccessResult {
    /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $access_group */
    foreach ($this->getNodeAccessGroups() as $name => $access_group) {
      if ($account->hasPermission($access_group->getPermissionName())) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

  /**
   * Get node access groups.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of node access group instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getNodeAccessGroups(): array {
    return $this->getEntityAccessGroupStorage()
      ->loadByProperties([
        'entity_type' => 'node',
      ]);
  }

  /**
   * Get entity access group storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityAccessGroupStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage('entity_access_group');
  }
}
