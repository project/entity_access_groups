<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the entity access rule default form.
 */
class EntityAccessGroupDefaultForm extends EntityForm {

  /**
   * @var string
   */
  public const ENTITY_REFERENCE_OPTION_ALL = 'all';

  /**
   * @var string
   */
  public const USER_REFERENCE_ACCESS_USER = 'user';

  /**
   * @var string
   */
  public const ENTITY_REFERENCE_OPTION_RELEVANT = 'relevant';

  /**
   * @var string
   */
  public const USER_REFERENCE_ACCESS_ONLY_ADMIN = 'only_admin';

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Entity access group default form constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info
  ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['#prefix'] = '<div id="entity-access-group">';
    $form['#suffix'] = '</div>';

    /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $entity->label(),
    ];
    $form['name'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Name'),
      '#machine_name' => [
        'exists' => [$entity, 'exists'],
      ],
      '#disabled' => !$entity->isNew(),
      '#default_value' => $entity->id(),
    ];
    $form['operations'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Access Operations'),
      '#description' => $this->t(
        'Select the entity access operations to restrict.'
      ),
      '#options' => $this->getEntityAccessOperationOptions(),
      '#required' => TRUE,
      '#default_value' => $entity->operations()
    ];

    $entity_type = $this->getFormStatePropertyValue(
      ['entity_type'],
      $form_state,
      $entity->entityType()
    );

    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Access Entity Type'),
      '#description' => $this->t(
        'Select the access group entity type.'
      ),
      '#options' => $this->getEntityTypeOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#required' => TRUE,
      '#default_value' => $entity_type,
      '#depth' => 1,
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => 'entity-access-group',
        'callback' => [$this, 'entityAccessGroupAjaxCallback'],
      ],
    ];

    if (isset($entity_type) && !empty($entity_type)) {
      $form['entity_bundles'] = [
        '#type' => 'select',
        '#title' => $this->t('Access Entity Bundles'),
        '#description' => $this->t('Select the access group entity bundles that
          you would like to restrict entity access to.'),
        '#required' => TRUE,
        '#multiple' => TRUE,
        '#options' => $this->getEntityBundleOptions($entity_type),
        '#default_value' => $entity->entityBundles()
      ];

      // Entity access configuration fieldset.
      $form['entity_access'] = [
        '#type' => 'details',
        '#title' => $this->t('Entity Access Configuration'),
        '#open' => TRUE,
        '#tree' => TRUE,
      ];
      $form['entity_access']['reference_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Entity Reference'),
        '#description' => $this->t(
          'Select the entity reference to determine the entity access.'
        ),
        '#required' => TRUE,
        '#empty_option' => $this->t('- Select -'),
        '#options' => $this->getEntityReferenceFieldOptions(
          $entity_type,
          ['entity_reference']
        ),
        '#default_value' => $entity->entityReferenceField()
      ];
      $form['entity_access']['reference_field_option'] = [
        '#type' => 'radios',
        '#title' => $this->t('Entity Reference Option'),
        '#description' => $this->t(
          'Select how entity reference options should be handled.'
        ),
        '#options' => [
          static::ENTITY_REFERENCE_OPTION_ALL => $this->t('Show All'),
          static::ENTITY_REFERENCE_OPTION_RELEVANT => $this->t('Show Only Relevant')
        ],
        '#required' => TRUE,
        '#default_value' => $entity->entityReferenceFieldOption(),
      ];
      $form['entity_access']['reference_field_required'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Entity Reference Required'),
        '#description' => $this->t(
          'Check if the entity reference should be mandatory.'
        ),
        '#default_value' => $entity->entityReferenceFieldRequired()
      ];
    }

    // User access configuration fieldset.
    $form['user_access'] = [
      '#type' => 'details',
      '#title' => $this->t('User Access Configuration'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['user_access']['reference_field'] = [
      '#type' => 'select',
      '#title' => $this->t('User Reference'),
      '#description' => $this->t('Select the user reference field to use to
        restrict entity access.'),
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select -'),
      '#options' => $this->getEntityReferenceFieldOptions(
        'user',
        ['entity_reference']
      ),
      '#default_value' => $entity->userReferenceField(),
    ];
    $form['user_access']['reference_field_access'] = [
      '#type' => 'radios',
      '#title' => $this->t('User Reference Field Access'),
      '#description' => $this->t('Select how user reference field access is
        handled.'),
      '#options' => [
        static::USER_REFERENCE_ACCESS_USER => $this->t(
          'User is allowed to manage groups.'
        ),
        static::USER_REFERENCE_ACCESS_ONLY_ADMIN => $this->t(
          'Only managers are allowed to manage groups.'
        ),
      ],
      '#required' => TRUE,
      '#default_value' => $entity->userReferenceFieldAccess()
    ];
    $form['user_access']['referenced_content_access'] = [
      '#type' => 'details',
      '#title' => $this->t('Referenced Content Access'),
      '#description' => $this->t('Restrict entity access on the referenced
        content. <br/> <strong>Note:</strong> If restricted, only administrator,
        users with the manage entity access group role, or users that are
        associated with the access group will be granted access.')
    ];
    $form['user_access']['referenced_content_access']['restrict'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Restrict Access'),
      '#default_value' => $entity->userReferencedContentAccessRestrict()
    ];
    $form['user_access']['referenced_content_access']['operations'] = [
      '#type' => 'select',
      '#title' => $this->t('Operations'),
      '#description' => $this->t('Select referenced content entity access
        operations to restrict.'),
      '#multiple' => TRUE,
      '#options' => $this->getEntityAccessOperationOptions(),
      '#states' => [
        'visible' => [
          ':input[name="user_access[referenced_content_access][restrict]"]' => ['checked' => TRUE]
        ]
      ],
      '#default_value' => $entity->userReferencedContentAccessOperations()
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $user_reference = $form_state->getValue(
      ['user_access', 'reference_field']
    );

    $entity_reference = $form_state->getValue(
      ['entity_access', 'reference_field']
    );

    $referenced_content_access = $form_state->getValue(
      ['user_access', 'referenced_content_access']
    );

    if (isset($referenced_content_access['restrict'])
      && $referenced_content_access['restrict']) {

      if (empty($referenced_content_access['operations'])) {
        $element = $form['user_access']['referenced_content_access']['operations'];
        $form_state->setError(
          $element,
          $this->t('@title is required!', ['@title' => $element['#title']])
        );
      }
    }

    if (isset($user_reference) && isset($entity_reference)) {
      $entity_type = $form_state->getValue('entity_type');

      if (isset($entity_type)) {
        $user_field_definitions = $this->entityFieldManager
          ->getFieldDefinitions('user', 'user');

        if (!isset($user_field_definitions[$user_reference])) {
          $form_state->setError(
            $form['user_access']['reference_field'],
            $this->t('User reference %name does not exist on the user entity.',
              ['%name' => $user_reference]
            )
          );
          return;
        }
        /** @var \Drupal\Core\Field\FieldDefinition $user_field_definition */
        $user_field_definition = $user_field_definitions[$user_reference];
        $user_field_target_bundles = $user_field_definition
            ->getSetting('handler_settings')['target_bundles'] ?? [];

        foreach ($form_state->getValue('entity_bundles', []) as $bundle) {
          $entity_field_definitions = $this->entityFieldManager
            ->getFieldDefinitions($entity_type, $bundle);

          if (!isset($entity_field_definitions[$entity_reference])) {
            $form_state->setError(
              $form['entity_access']['reference_field'],
              $this->t('Entity reference %name does not exist on the bundle
                %bundle.', ['%name' => $entity_reference, '%bundle' => $bundle]
              )
            );
            return;
          }
          $entity_field_definition = $entity_field_definitions[$entity_reference];
          $entity_field_target_bundles = $entity_field_definition
              ->getSetting('handler_settings')['target_bundles'] ?? [];

          foreach ($entity_field_target_bundles as $entity_target_bundle) {
            if (!isset($user_field_target_bundles[$entity_target_bundle])) {
              $form_state->setError(
                $form['user_access']['reference_field'],
                $this->t('User reference does not have the target bundle @bundle
                defined.', ['@bundle' => $entity_target_bundle])
              );
              return;
            }
          }
        }
      }
    }
  }

  /**
   * Entity access group ajax callback.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   An array of the form elements.
   */
  public function entityAccessGroupAjaxCallback(
    array $form,
    FormStateInterface $form_state
  ): array {
    $element = $form_state->getTriggeringElement();

    $parents = isset($element['#depth']) && $element['#depth'] > 0
      ? array_slice($element['#array_parents'], 0, "-{$element['#depth']}")
      : $element['#array_parents'];

    return NestedArray::getValue($form, $parents);
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $status = parent::save($form, $form_state);

    /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $entity */
    $entity = $this->entity;
    $action = $status === SAVED_NEW ? 'added' : 'updated';

    $this->messenger()->addStatus(
      $this->t("The @entity_type %label has been @action!", [
        '@action' => $action,
        '%label' => $entity->label(),
        '@entity_type' => $entity->getEntityType()->getLabel(),
      ])
    );

    $form_state->setRedirectUrl($entity->toUrl('collection'));

    return $status;
  }

  /**
   * Get form state property value.
   *
   * @param $key
   *   Either a string or an array of the nested structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   * @param null $default_value
   *   The default property value.
   *
   * @return array|mixed|null
   */
  protected function getFormStatePropertyValue(
    $key,
    FormStateInterface $form_state,
    $default_value = NULL
  ) {
    $key = !is_array($key) ? [$key] : $key;

    $inputs = [
      $form_state->getValues(),
      $form_state->getUserInput(),
    ];

    foreach ($inputs as $input) {
      $key_exists = NULL;
      $value = NestedArray::getValue($input, $key, $key_exists);

      if ($key_exists) {
        return $value;
      }
    }

    return $default_value;
  }

  /**
   * Get entity access operation options.
   *
   * @return array
   */
  protected function getEntityAccessOperationOptions(): array {
    return [
      'view' => $this->t('View'),
      'create' => $this->t('Create'),
      'update' => $this->t('Update'),
      'delete' => $this->t('Delete'),
    ];
  }

  /**
   * Get entity reference field options.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param array $field_types
   *   An array of the field types allowed.
   *
   * @return array
   *   An array of entity reference field options.
   */
  protected function getEntityReferenceFieldOptions(
    string $entity_type_id,
    array $field_types
  ): array {
    $options = [];
    $definitions = $this->filterFieldStorageDefinitions(
      $entity_type_id,
      $field_types
    );

    /** @var \Drupal\field\Entity\FieldStorageConfig $definition */
    foreach ($definitions as $field_name => $definition) {
      $options[$field_name] = $definition->getName();
    }

    return $options;
  }

  /**
   * Filter field storage definitions by field types.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param array $field_types
   *   An array of the field types allowed.
   *
   * @return array
   *   An array of filtered field storage definitions.
   */
  protected function filterFieldStorageDefinitions(
    string $entity_type_id,
    array $field_types
  ): array {
    $definitions = [];

    $entity_field_storage_definitions = $this->entityFieldManager
      ->getFieldStorageDefinitions($entity_type_id);

    foreach ($entity_field_storage_definitions as $field_name => $definition) {
      if (!$definition instanceof FieldStorageConfigInterface
        || !in_array($definition->getType(), $field_types)) {
        continue;
      }
      $definitions[$field_name] = $definition;
    }

    return $definitions;
  }

  /**
   * Get the entity bundle options.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   An array of entity bundle options.
   */
  protected function getEntityBundleOptions(string $entity_type_id): array {
    $options = [];

    foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type_id) as $bundle => $info) {
      if (!isset($info['label'])) {
        continue;
      }
      $options[$bundle] = $info['label'];
    };

    return $options;
  }

  /**
   * Get entity type options list.
   *
   * @return array
   *   Get the content entity type options.
   */
  protected function getEntityTypeOptions(): array {
    $options = [];

    foreach ($this->entityTypeManager->getDefinitions() as $plugin_id => $definition) {
      if (!$definition instanceof ContentEntityTypeInterface) {
        continue;
      }
      $options[$plugin_id] = $definition->getLabel();
    }

    return $options;
  }
}
