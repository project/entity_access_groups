<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityDeleteFormTrait;

/**
 * Define the entity access rule delete form.
 */
class EntityAccessGroupDeleteForm extends EntityConfirmFormBase {
  use EntityDeleteFormTrait;
}
