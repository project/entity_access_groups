<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Form;

use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_access_groups\Contract\EntityAccessGroupInterface;

/**
 * Define the entity access group user remove form.
 */
class EntityAccessGroupUserRemoveForm extends ConfirmFormBase {

  /**
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * @var \Drupal\node\Entity\Node
   */
  protected $node;

  /**
   * @var \Drupal\entity_access_groups\Entity\EntityAccessGroup
   */
  protected $entityAccessGroup;

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'entity_access_group_user_remove_form';
  }

  /**
   * {@inheritDoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Remove %username from the %label access group?', [
      '%username' => $this->user->getAccountName(),
      '%label' => $this->entityAccessGroup->label()
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('entity_access_groups.node.local_task', [
      'node' => $this->node->id()
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    NodeInterface $node = NULL,
    UserInterface $user = NULL,
    EntityAccessGroupInterface $entity_access_group = NULL
  ): array {
    $this->user = $user;
    $this->node = $node;
    $this->entityAccessGroup = $entity_access_group;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $reference_field = $this->entityAccessGroup->userReferenceField();

    if ($this->user->hasField($reference_field)) {
      $node_id = $this->node->id();

      $field_item = $this->user->get($reference_field);
      $field_index = $this->searchFieldItemValueIndex($node_id, $field_item);

      if (FALSE !== $field_index) {
        try {
          $field_item->removeItem($field_index);
          $this->user->save();
        } catch (\Exception $exception) {
          watchdog_exception('entity_access_group', $exception);
        }
      }
    }
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * Search user reference value index.
   *
   * @param string $match_value
   *   The field match value.
   * @param \Drupal\Core\Field\FieldItemListInterface $field_item
   *   The field item instance.
   *
   * @return bool|int
   *   Return the matched value index; otherwise FALSE if not found.
   */
  protected function searchFieldItemValueIndex(
    string $match_value,
    FieldItemListInterface $field_item
  ) {
    $reference_field_property = $field_item->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getMainPropertyName();

    foreach ($field_item->getValue() as $index => $values) {
      if (!isset($values[$reference_field_property])) {
        continue;
      }

      if ($values[$reference_field_property] == $match_value) {
        return (int) $index;
      }
    }

    return FALSE;
  }
}
