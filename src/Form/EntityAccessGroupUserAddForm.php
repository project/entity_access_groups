<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Form;

use Drupal\Core\Form\FormBase;
use Drupal\node\NodeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\entity_access_groups\Contract\EntityAccessGroupInterface;

/**
 * Define the entity access group user form.
 */
class EntityAccessGroupUserAddForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Define the entity access group user form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'entity_access_group_user_add_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    NodeInterface $node = NULL,
    EntityAccessGroupInterface $entity_access_group = NULL
  ): array {
    $form['#prefix'] = '<div id="entity-access-group-user-form">';
    $form['#suffix'] = '</div>';

    $form['search'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Search Users'),
      '#tree' => TRUE,
    ];
    $form['search']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
    ];
    $form['search']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Search'),
      '#limit_validation_errors' => [['search']],
      '#ajax' => [
        'event' => 'click',
        'method' => 'replace',
        'wrapper' => 'entity-access-group-user-form',
        'callback' => [$this, 'accessGroupUserAjaxCallback'],
      ],
    ];

    $username = $this->getFormStatePropertyValue(
      ['search', 'username'],
      $form_state
    );

    $form['users'] = [
      '#type' => 'tableselect',
      '#header' => [
        $this->t('Username'),
        $this->t('Email'),
      ],
      '#options' => $this->searchUsersByUsername($username),
      '#empty' => $this->t('No users matching the search criteria.'),
    ];
    $form['#node'] = $node;
    $form['#entity_access_group'] = $entity_access_group;

    $form['actions']['#type'] = 'actions';
    $form['actions']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add User'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!isset($form['#node']) || !isset($form['#entity_access_group'])) {
      return;
    }
    /** @var \Drupal\node\Entity\Node $node */
    $node = $form['#node'];
    /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $entity_access_group */
    $entity_access_group = $form['#entity_access_group'];

    if ($user_ids = array_filter($form_state->getValue('users', []))) {
      $user_reference_field = $entity_access_group->userReferenceField();

      /** @var \Drupal\user\Entity\User $user */
      foreach ($this->getUserStorage()->loadMultiple($user_ids) as $user) {
        if (!$user->hasField($user_reference_field)) {
          continue;
        }
        $user->set($user_reference_field, $node->id());
        $user->save();
      }
    }

    $form_state->setRedirect(
      'entity_access_groups.node.local_task',
      ['node' => $node->id()]
    );
  }

  /**
   * Access group user ajax callback.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   An array of the form elements.
   */
  public function accessGroupUserAjaxCallback(
    array $form,
    FormStateInterface $form_state
  ): array {
    return $form;
  }

  /**
   * Search users by the username.
   *
   * @param $username
   *   The username to search.
   *
   * @return array
   *   An array of instantiated users matching the username.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function searchUsersByUsername($username): array {
    $options = [];

    if (isset($username) && !empty($username)) {
      $user_ids = $this->getUserStorage()
        ->getQuery()
        ->accessCheck()
        ->condition('status', TRUE)
        ->condition('name', $username, 'CONTAINS')
        ->sort('name', 'ASC')
        ->execute();

      /** @var \Drupal\user\Entity\User $user */
      foreach ($this->getUserStorage()->loadMultiple($user_ids) as $user_id => $user) {
        $options[$user_id][] = [
          'username' => $user->getAccountName(),
          'email' => $user->getEmail(),
        ];
      }
    }

    return $options;
  }

  /**
   * Get form state property value.
   *
   * @param $key
   *   Either a string or an array of the nested structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   * @param null $default_value
   *   The default property value.
   *
   * @return array|mixed|null
   */
  protected function getFormStatePropertyValue(
    $key,
    FormStateInterface $form_state,
    $default_value = NULL
  ) {
    $key = !is_array($key) ? [$key] : $key;

    $inputs = [
      $form_state->getValues(),
      $form_state->getUserInput(),
    ];

    foreach ($inputs as $input) {
      $key_exists = NULL;
      $value = NestedArray::getValue($input, $key, $key_exists);

      if ($key_exists) {
        return $value;
      }
    }

    return $default_value;
  }

  /**
   * Get the user entity storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getUserStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('user');
  }
}
