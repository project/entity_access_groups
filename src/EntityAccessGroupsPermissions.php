<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Define the entity access groups permissions
 */
class EntityAccessGroupsPermissions {

  use StringTranslationTrait;

  /**
   * Define the entity access groups permissions.
   *
   * @return array
   *   An array of generated entity access group permissions.
   */
  public function permissions(): array {
    $permissions = [];

    try {
      $access_groups = $this->getEntityAccessGroupStorage()->loadMultiple();

      /** @var \Drupal\entity_access_groups\Entity\EntityAccessGroup $access_group */
      foreach($access_groups as $name => $access_group) {
        $permissions[$access_group->getPermissionName()] = [
          'title' => $this->t('Manage %label entity access group', [
            '%label' => $access_group->label()
          ]),
        ];
      }
    } catch (\Exception $exception) {
      watchdog_exception('entity_access_groups', $exception);
    }

    return $permissions;
  }

  /**
   * Get entity access group storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   Return the entity access group storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityAccessGroupStorage(): EntityStorageInterface {
    return \Drupal::entityTypeManager()->getStorage('entity_access_group');
  }
}
