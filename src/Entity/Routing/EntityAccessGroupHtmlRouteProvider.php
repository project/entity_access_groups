<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Entity\Routing;

use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;

/**
 * Define entity access group HTML route provider.
 */
class EntityAccessGroupHtmlRouteProvider extends DefaultHtmlRouteProvider {}
