<?php

declare(strict_types=1);

namespace Drupal\entity_access_groups\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\entity_access_groups\EntityExistTrait;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\entity_access_groups\Form\EntityAccessGroupDefaultForm;
use Drupal\entity_access_groups\Contract\EntityAccessGroupInterface;

/**
 * Define configuration entity to collect the entity access group.
 *
 * @ConfigEntityType(
 *   id = "entity_access_group",
 *   label = @Translation("Entity access group"),
 *   label_plural = @Translation("Entity access groups"),
 *   label_singular = @Translation("Entity access group"),
 *   label_collection = @Translation("Entity access groups"),
 *   admin_permission = "administer entity access groups",
 *   config_prefix = "group",
 *   config_export = {
 *     "name",
 *     "label",
 *     "operations",
 *     "entity_type",
 *     "user_access",
 *     "entity_access",
 *     "entity_bundles"
 *   },
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\entity_access_groups\Form\EntityAccessGroupDefaultForm",
 *       "edit" = "\Drupal\entity_access_groups\Form\EntityAccessGroupDefaultForm",
 *       "delete" = "\Drupal\entity_access_groups\Form\EntityAccessGroupDeleteForm",
 *       "default" = "\Drupal\entity_access_groups\Form\EntityAccessGroupDefaultForm"
 *     },
 *     "list_builder" = "\Drupal\entity_access_groups\Controller\EntityAccessGroupListBuilder",
 *     "route_provider" = {
 *       "html" = "\Drupal\entity_access_groups\Entity\Routing\EntityAccessGroupHtmlRouteProvider"
 *     }
 *   },
 *   links = {
 *     "collection" = "/admin/config/content/entity-access-groups",
 *     "add-form" = "/admin/config/content/entity-access-groups/add",
 *     "edit-form" = "/admin/config/content/entity-access-groups/{entity_access_group}",
 *     "delete-form" = "/admin/config/content/entity-access-groups/{entity_access_group}/delete"
 *   }
 * )
 */
class EntityAccessGroup extends ConfigEntityBase implements EntityAccessGroupInterface {

  use EntityExistTrait;

  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $label;

  /**
   * @var array
   */
  protected $operations = [];

  /**
   * @var string
   */
  protected $entity_type;

  /**
   * @var array
   */
  protected $entity_bundles = [];

  /**
   * @var array
   */
  protected $user_access = [];

  /**
   * @var array
   */
  protected $entity_access = [];

  /**
   * {@inheritDoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritDoc}
   */
  public function operations(): array {
    return array_filter($this->operations);
  }

  /**
   * {@inheritDoc}
   */
  public function entityType(): string {
    return $this->entity_type ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function entityBundles(): array {
    return $this->entity_bundles ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function hasEntityBundles(): bool {
    return !empty($this->entityBundles());
  }

  /**
   * {@inheritDoc}
   */
  public function entityReferenceField(): string {
    return $this->entityAccess()['reference_field'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function entityReferenceFieldOption(): string {
    return $this->entityAccess()['reference_field_option'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function entityReferenceFieldRequired(): bool {
    return (bool) ($this->entityAccess()['reference_field_required'] ?? FALSE);
  }

  /**
   * {@inheritDoc}
   */
  public function userReferenceField(): string {
    return $this->userAccess()['reference_field'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function userReferenceFieldAccess(): string {
    return $this->userAccess()['reference_field_access'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function userReferencedContentAccessRestrict(): bool {
    return (bool) ($this->userAccess()['referenced_content_access']['restrict'] ?? FALSE);
  }

  /**
   * {@inheritDoc}
   */
  public function userReferencedContentAccessOperations(): array {
    return $this->userAccess()['referenced_content_access']['operations'] ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function getPermissionName(): string {
    return "manage {$this->name} entity access group";
  }

  /**
   * Get entity access configuration.
   *
   * @return array
   *   An array of the entity access configuration.
   */
  protected function entityAccess(): array {
    return !empty($this->entity_access) ? $this->entity_access : [
      'reference_field' => NULL,
      'reference_field_option' => EntityAccessGroupDefaultForm::ENTITY_REFERENCE_OPTION_ALL,
    ];
  }

  /**
   * Get user access configuration.
   *
   * @return array
   *   An array of the user access configuration.
   */
  protected function userAccess(): array {
    return !empty($this->user_access) ? $this->user_access : [
      'reference_field' => NULL,
      'reference_field_access' => EntityAccessGroupDefaultForm::USER_REFERENCE_ACCESS_ONLY_ADMIN,
      'referenced_content_access' => [
        'restrict' => FALSE,
        'operations' => []
      ]
    ];
  }
}
